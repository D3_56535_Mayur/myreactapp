import { v4 as uuidv4 } from "uuid";

import FeedbackList from "./components/FeedbackList";
import { useState } from "react";
import FeedbackData from "./data/feddbackdata";
import FeedbackStats from "./components/FeedbackStats";
import Feedbackform from "./components/Feedbackform";
import About from "./pages/About";
//import { FeedbackProvider } from "./components/Context/FeedbackContext";
import AboutIconLink from "./components/AboutIconLink";
import { BrowserRouter as Router, Route, Routes} from "react-router-dom";

function App() {
  const [feedback, setFeedback] = useState(FeedbackData);
  const deleteFeedback = (id) => {
    setFeedback(feedback.filter((item) => item.id !== id));
  };
  const addFeedback = (newFeedback) => {
    newFeedback.id = uuidv4();
    setFeedback([newFeedback, ...feedback]);
  };
  return (
    // <FeedbackProvider>
      <Router>
        <div className="container">
      <header></header>
      <Routes>
      <Route path="/about" element={<About />}></Route>
        <Route
          path="/"
          element={
            <div>
              <Feedbackform handleAdd={addFeedback} />
              <FeedbackStats feedback={feedback} />
              <FeedbackList feedback={feedback} handleDelete={deleteFeedback} />
              
            </div>
          }
        ></Route>
       
      </Routes>
      <AboutIconLink/>
      </div>
      </Router>
      // </FeedbackProvider>
  );
}

export default App;
// if we have to pass params to the next component through url 
// then we can write uri of that component as "/uri/:id"
//and in the "uri" comp we can import "useParam" and extract id
//we can pass multiple params through url
//URL = http://localhost/3000/uri/2

//Also we can add routes in any of the component by importing Routes and Routes in that component in that case in app.js we have to mention path of that Route as 
//  path = '/component/*'