import Card from "../components/shared/Card"
import {Link} from 'react-router-dom'

function About() {
    return (
        <Card>
            <div className="about">
                <h1>About this Project</h1>
                <p>Thsi is react app to leave feedback for a product or service

                </p>
                <p>Version: 1.0.0</p>
                <p><Link to='/'>Back To Home--</Link></p>
                {/* we can pass multiple query params through link
                by writing inside inside to={
                    pathname : '/link',
                    Search: 20,
                    etc... 

                    <NavLink> can be used instead of Link where it has option of applying css style to 
                    link which is active since it has attribute "activeClassName"
                } */}
            </div>
            
        </Card>
    )
}

export default About
